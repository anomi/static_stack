#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch2/catch.hpp"
#include "static_stack/static_stack.h"

TEST_CASE("static stack reserve macro", "[static_stack, sstack_reserve]") {
    /**
     * Unfortunately this are more alibi tests because C does not proved better options
     */
    SECTION( "static stack reserve normal" ) { 
        REQUIRE(sstack_reserve(10, 1) == (10 * 1 + sizeof(sstack_header_t)));
    }
    SECTION("static stack reserve with 0 capacity") {
        REQUIRE(sstack_reserve(0, 1) == (0 * 1 + sizeof(sstack_header_t)));
    }
    SECTION("static stack reserve with negative capacity") {
        REQUIRE(sstack_reserve(-10000000, 1) == (-10000000 * 1 + sizeof(sstack_header_t)));
    }
    SECTION("static stack reserve with 0 element size") {
        REQUIRE(sstack_reserve(10, 0) == (10 * 0 + sizeof(sstack_header_t)));
    }
    SECTION("static stack reserve with negativ element size") {
        REQUIRE(sstack_reserve(10, -10000000) == (10 * -10000000 + sizeof(sstack_header_t)));
    }
}

TEST_CASE("static stack init function tests", "[static_stack, sstack_init]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));

    SECTION("static stack init normal") {
        REQUIRE(true == sstack_init(s, capacity, elem_size));
    }
    SECTION("static stack init with nullptr") {
        REQUIRE(false == sstack_init(NULL, capacity, elem_size));
    }
    SECTION("static stack init with 0 capacity") {
        REQUIRE(false == sstack_init(s, 0, elem_size));
    }
    SECTION("static stack init with 0 element size") {
        REQUIRE(false == sstack_init(s, capacity, 0));
    }
}

TEST_CASE("static stack push function tests", "[static_stack, sstack_push]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == sstack_init(s, capacity, elem_size));

    SECTION("static stack push normal") {
        int i = 0;
        while (true == sstack_push(s, &i))
        {
            i++;
        }

        REQUIRE(i == capacity);
    }
    SECTION("static stack push with stack nullptr") {
        int i = 0;
        while (true == sstack_push(NULL, &i))
        {
            i++;
        }

        REQUIRE(i == 0);
    }
    SECTION("static stack push with element nullptr") {
        int i = 0;
        while (true == sstack_push(s, NULL))
        {
            i++;
        }

        REQUIRE(i == 0);
    }
    SECTION("static stack push with stack not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}

TEST_CASE("static stack pop function tests", "[static_stack, sstack_pop]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == sstack_init(s, capacity, elem_size));
    int i = 0;
    for (i = 0; true == sstack_push(s, &i); i++);
    REQUIRE(i == capacity);

    SECTION("static stack pop normal") {
        int i = 0;
        int e = 0;
        while (true == sstack_pop(s, &e))
        {
            i++;
        }

        REQUIRE(i == capacity);
    }
    SECTION("static stack pop with stack nullptr") {
        int i = 0;
        while (true == sstack_pop(NULL, &i))
        {
            i++;
        }

        REQUIRE(i == 0);
    }
    SECTION("static stack pop with element nullptr") {
        int i = 0;
        while (true == sstack_pop(s, NULL))
        {
            i++;
        }

        REQUIRE(i == 0);
    }
    SECTION("static stack pop with stack not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}

TEST_CASE("static stack empty function tests", "[static_stack, sstack_empty]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    bool empty = true;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == sstack_init(s, capacity, elem_size));

    SECTION("static stack empty with empty stack") {
        REQUIRE(sstack_empty(s, &empty) == true);
        REQUIRE(empty == true);
    }
    SECTION("static stack empty with not empty stack") {
        int e = 42;
        REQUIRE(sstack_push(s, &e) == true);
        REQUIRE(sstack_empty(s, &empty) == true);
        REQUIRE(empty == false);
    }
    SECTION("static stack empty with stack nullptr") {
        REQUIRE(sstack_empty(NULL, &empty) == false);
    }
    SECTION("static stack empty with element nullptr") {
        REQUIRE(sstack_empty(s, NULL) == false);
    }
    SECTION("static stack empty with stack not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}

TEST_CASE("static stack clear function tests", "[static_stack, sstack_clear]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == sstack_init(s, capacity, elem_size));
    bool empty = false;

    SECTION("static stack clear with full stack") {
        int i = 0;
        for (i = 0; true == sstack_push(s, &i); i++);
        REQUIRE(i == capacity);
        REQUIRE(true == sstack_clear(s));
        REQUIRE(true == sstack_empty(s, &empty));
        REQUIRE(true == empty);
    }
    SECTION("static stack clear with empty stack") {
        REQUIRE(true == sstack_clear(s));
        REQUIRE(true == sstack_empty(s, &empty));
        REQUIRE(true == empty);
    }
    SECTION("static stack clear with stack nullptr") {
        REQUIRE(false == sstack_clear(NULL));
    }
    SECTION("static stack clear with stack not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}

TEST_CASE("static stack size function tests", "[static_stack, sstack_size]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == sstack_init(s, capacity, elem_size));
    size_t size = 0;

    SECTION("static stack size with full stack") {
        for (int i = 0; true == sstack_push(s, &i); i++);
        REQUIRE(true == sstack_size(s, &size));
        REQUIRE(capacity == size);
    }
    SECTION("static stack size with half full stack") {
        for (int i = 0; (i < (int)(capacity / 2)) && (true == sstack_push(s, &i)); i++);
        REQUIRE(true == sstack_size(s, &size));
        REQUIRE((capacity / 2) == size);
    }
    SECTION("static stack size with empty stack") {
        REQUIRE(true == sstack_size(s, &size));
        REQUIRE(0 == size);
    }
    SECTION("static stack size with stack nullptr") {
        REQUIRE(false == sstack_size(NULL, &size));
    }
    SECTION("static stack size with element nullptr") {
        REQUIRE(false == sstack_size(s, NULL));
    }
    SECTION("static stack size with stack not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}

TEST_CASE("static stack capacity function tests", "[static_stack, sstack_capacity]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    sstack_t s[sstack_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(s, 0, sizeof(s)));
    REQUIRE(true == sstack_init(s, capacity, elem_size));
    size_t c = 42;

    SECTION("static stack capacity normal") {
        REQUIRE(true == sstack_capacity(s, &c));
        REQUIRE(capacity == c);
    }
    SECTION("static stack capacity with nullptr") {
        REQUIRE(false == sstack_capacity(NULL, &c));
    }
    SECTION("static stack capacity with nullptr capacity") {
        REQUIRE(false == sstack_capacity(s, NULL));
    }
    SECTION("static stack capacity with stack not initialized") {
        /* not implemented */
        REQUIRE(true == true);
    }
}
