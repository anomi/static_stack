# What is Static Stack ?

Static Stack or short sstack is a mini library, which implements a stack. It uses no heap allocations and the size is fixed at compile time. The stack can store any size of an element and a speciefied amount of elements.

## How to use

A basic example can be found below. For more information see the documentation in the header file.

```C
#include <stdio.h>
#include <stdbool.h>
#include "static_stack/static_stack.h"

int main(int argc, char const *argv[])
{
    // reserve enough memory
    sstack_t my_stack[sstack_reserve(10, sizeof(unsigned int))];
    // init the internal structure of the stack
    (void)sstack_init(my_stack, 10, sizeof(unsigned int));

    // fill up the stack until it's full
    for (unsigned int i = 0; true == sstack_push(my_stack, &i); i++)
    {
        printf("pushed %u into the stack\n", i);
    }

    // get the stored values from the stack
    for (unsigned int i = 0; true == sstack_pop(my_stack, &i);)
    {
        printf("popped %u, from the stack\n", i);

        if (i == 5)
        {
            // just delete everythin else 
            (void)sstack_clear(my_stack);
        }
    }

    return 0;
}
```

# Why Using This Approach For Creating a Stack

The desinge goal was to have a stack implemenation without a heap. If you want to achive this you could also to the following:

```C
typedef struct {
    uint8_t* data;
    size_t index;
    size_t capacity;
    size_t elem_size;
} stack_t;

bool stack_init(stack_t* s, uint8_t* mem, size_t capacity, size_t elem_size);
```

This approach has the problem that an external array has to be provieded to the stack. I wanted to bundle the stack structure with it's own memory. Because of that I use the approach of creating a plain array of `uint8_t` and put there a header namely `sstack_header_t` and the elements afterwords. Of cause this has the drawback that I have to implement a memory handling for the array.

But not only that you have also the problem that if you would may the array to big in size the programm would crash because you would run out of stack. To avoid this you could declare the data array static. But be patient in what you are implying by that.

## How to Build

1. `$> git clone git@gitlab.com:anomi/static_stack.git`
2. `$> cd static_stack`
3. `$> git submodule update --init --recursive`
4. `$> mkdir build`
5. `$> cd build`
6. `$> cmake ../ && make`
