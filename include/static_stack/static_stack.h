#ifndef _STATIC_STACK_H_
#define _STATIC_STACK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * @brief header of the stack memory used internaly
 * 
 */
typedef struct {
    size_t size;        /**< number of elements are stored */
    size_t capacity;    /**< max number of elements can be stored */
    size_t elem_size;   /**< size of one element in byte */
} sstack_header_t;

/**
 * @brief type alias for the static stack
 * 
 */
typedef uint8_t sstack_t;

/**
 * @brief helper macro for reserving enought memory
 * 
 */ 
#define sstack_reserve(capacity, elem_size) (((size_t)(capacity)) * (size_t)(elem_size) + sizeof(sstack_header_t))

/**
 * @brief init the stack
 * 
 * @param s stack instance
 * @param capacity specifie the number of elements the stack can hold.
 * @param elem_size specifie the size of one element in byte
 * @sa sstack_reserve
 * @note capacity && elem_size musst be the same as in sstack_reserve macro
 * @return true on success  
 */
bool sstack_init(sstack_t* s, size_t capacity, size_t elem_size);

/**
 * @brief push an element onto the stack
 * 
 * @param s stack instance
 * @param e pointer to memory where the element is copied from
 * @return true on success  
 */
bool sstack_push(sstack_t* s, void* e);

/**
 * @brief pop the last added element from the stack
 * 
 * @param s stack instance
 * @param e pointer to memory where the element is copied to
 * @return true on success
 */
bool sstack_pop(sstack_t* s, void* e);

/**
 * @brief check if the stack is empty
 * 
 * @param s stack instance
 * @param empty pointer to memory where the bool will be stored
 * @return true on success 
 */
bool sstack_empty(sstack_t* s, bool* empty);

/**
 * @brief empty the stack
 * 
 * @param s stack instance
 * @return true on success 
 */
bool sstack_clear(sstack_t* s);

/**
 * @brief get the number of elements the stack is holding
 * 
 * @param s stack instance
 * @param size pointer to memory where the size will be stored
 * @return true on success  
 */
bool sstack_size(sstack_t* s, size_t* size);

/**
 * @brief get the max number of elements the stack can hold
 * 
 * @param s stack instance
 * @param capacity pointer to memory where the capacity will be stored
 * @return true on success
 */
bool sstack_capacity(sstack_t* s, size_t* capacity);

#ifdef __cplusplus
}
#endif

#endif // _STATIC_STACK_H_