#include "static_stack/static_stack.h"
#include <string.h>

bool sstack_init(sstack_t* s, size_t capacity, size_t elem_size) {
    bool b = false;
    sstack_header_t* h = (sstack_header_t*)s;
    if (s != NULL
        && elem_size > 0
        && capacity > 0) 
    {
        h->size = 0;
        h->capacity = capacity;
        h->elem_size = elem_size;
        b = true;
    }
    return b;
}

bool sstack_push(sstack_t* s, void* e) {
    bool b = false;
    uint8_t* data = (uint8_t*)(s + sizeof(sstack_header_t));
    sstack_header_t* h = (sstack_header_t*)s;
    if (s != NULL
        && e != NULL
        && h->size < h->capacity)
    {
        if (memcpy(&data[h->size * h->elem_size], e, h->elem_size) != NULL) 
        {
            b = true;
            h->size++;
        }
    }
    return b;
}

bool sstack_pop(sstack_t* s, void* e) {
    bool b = false;
    uint8_t* data = (uint8_t*)(s + sizeof(sstack_header_t));
    sstack_header_t* h = (sstack_header_t*)s;
    if (s != NULL
        && e != NULL
        && h->size > 0) 
    {
        size_t tmp = h->size - 1;
        if (memcpy(e, &data[tmp * h->elem_size], h->elem_size) != NULL)
        {
            h->size--;
            b = true;
        }
    }
    return b;
}

bool sstack_empty(sstack_t* s, bool* empty) {
    bool b = false;
    sstack_header_t* h = (sstack_header_t*)s;
    if (s != NULL
        && empty != NULL) 
    {
        *empty = h->size == 0 ? true : false;
        b = true;
    }
    return b;
}

bool sstack_clear(sstack_t* s) {
    sstack_header_t* h = (sstack_header_t*)s;
    return s != NULL ? sstack_init(s, h->capacity, h->elem_size) : false;
}

bool sstack_size(sstack_t* s, size_t* size) {
    bool b = false;
    sstack_header_t* h = (sstack_header_t*)s;
    if (s != NULL
        && size != NULL) 
    {
        *size = h->size;
        b = true;
    }
    return b;
}

bool sstack_capacity(sstack_t* s, size_t* capacity) {
    bool b = false;
    sstack_header_t* h = (sstack_header_t*)s;
    if (s != NULL
        && capacity != NULL) 
    {
        *capacity = h->capacity;
        b = true;
    }
    return b;
}
